# Les bases de Docker

Avant toute chose, rendez-vous sur https://gitlab.univ-lille.fr/2021-lc512-virtu-et-container/ pour y créer un Dépot à votre nom avec à l'intérieur 1 dossier par TP. Vous allez maintenant travailler dans ce dépot git.

## Context

En trinomes, vous avez 1h pour me préparer une prez sur l'ensemble de ces sujets, dans 1h vous aurez 5 min pour présenter à toute la classe l'un de ces chapitres.

* c'est quoi la virtualisation
* c'est quoi la paravirtualisation
* c'est quoi les containers systèmes
* c'est quoi les containers applicatifs
* c'est quoi l'ochestration de containers


Je suis à votre disposition pour vous aider à construire vos présentations.

N'oubliez pas d'aborder l'histoire, les outils, les technologies sous jacentes, avantages, inconvénients, etc.

# Docker images

Toute la suite se fait seul

## Q1

Tester les commandes suivantes plusieurs fois
    
    docker run hello-world 
    docker run ubuntu date
    
et visiter https://hub.docker.com/_/hello-world, compléter le diagrame de séquence pour expliquer ce qui c'est passé depuis le lancement de la commande `docker run hello-world` jusqu'au retour dans le shell.

```mermaid
sequenceDiagram
    docker->>dockerd: ?
    docker->>hub docker: ?
    docker->>cache locale: ?
```

